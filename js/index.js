window.onscroll = () => {
  const nav = document.querySelector('.navbar');
  if (this.scrollY <= 600) nav.style.backgroundColor = '';
  else nav.style.backgroundColor = '#7fb3d5';
};

function submit() {
  let firstName = document.getElementById('firstname').value.toString();
  let lastName = document.getElementById('lastname').value.toString();
  let address = document.getElementById('address').value.toString();
  let birthday = document.getElementById('birthday').value.toString();
  let contact = document.getElementById('contact').value.toString();
  let email = document.getElementById('email').value.toString();
  let size = document.getElementById('size').value.toString();
  let gender = document.getElementById('gender').value.toString();
  let checkbox = document.getElementById('terms').checked;
  let datetime = new Date().toLocaleString();
  let error = document.querySelector('.error-message');
  let messages = [];
  let participants = [];
  let data = {
    firstname: firstName,
    lastname: lastName,
    address: address,
    birthday: birthday,
    contract: contact,
    email: email,
    size: size,
    datetime: datetime,
    gender: gender,
  };

  //Validation
  if (firstName === '') {
    if (!messages.includes('First Name Missing*')) {
      messages.push('First Name Missing*');
    } else return;
  }

  if (lastName === '') {
    if (!messages.includes('Last Name Missing*')) {
      messages.push('Last Name Missing*');
    }
  }

  if (address === '') {
    if (!messages.includes('Address Missing*')) {
      messages.push('Address Missing*');
    }
  }

  if (birthday === '') {
    if (!messages.includes('Birthdate Missing*')) {
      messages.push('Birthdate Missing*');
    }
  }

  if (!validateEmail(email)) {
    if (!messages.includes('Please Enter a Valid Email*')) {
      messages.push('Please Enter a Valid Email*');
    }
  }

  if (gender === '0') {
    if (!messages.includes('Gender Field Missing*')) {
      messages.push('Gender Field Missing*');
    }
  }

  if (size === '0') {
    if (!messages.includes('Size Missing*')) {
      messages.push('Size Field Missing*');
    }
  }

  if (!checkbox) {
    if (!messages.includes('Please check Terms & Conditions*')) {
      messages.push('Please check Terms & Conditions*');
    }
  }

  if (messages.length > 0) {
    document.querySelector('.error-message').innerHTML = '';
    messages.forEach(function (value, index) {
      let node = document.createElement('LI');
      let textnode = document.createTextNode(value);
      let errorUL = document.querySelector('.error-message');
      node.appendChild(textnode);
      errorUL.appendChild(node);
    });
  } else {
    participants.push(data);
    localStorage.setItem('participants', JSON.stringify(participants));
    setTimeout((window.location.href = './results-page.html'), 5000);
  }
}

function gotoSection() {
  document.getElementById('section').scrollIntoView();
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
