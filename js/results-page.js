function getData() {
  let data = JSON.parse(localStorage.getItem('participants'));
  let index = data.length == 1 ? 0 : data.length - 1;
  document.querySelector('.fname').textContent =
    data[index].firstname + ' ' + data[index].lastname;
  document.querySelector('.size').textContent = data[index].size;
  document.querySelector('.gender').textContent = data[index].gender;
  document.querySelector('.datetime').textContent = data[index].datetime;
}

function goBack() {
  window.location.href = './index.html';
}
